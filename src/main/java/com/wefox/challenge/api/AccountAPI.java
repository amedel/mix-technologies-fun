package com.wefox.challenge.api;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wefox.challenge.service.AccountService;
import com.wefox.challenge.vo.AccountVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/accounts")
@Slf4j
@RequiredArgsConstructor
public class AccountAPI {
	private final AccountService accountService;

	@GetMapping
    public ResponseEntity<List<AccountVO>> findAll() {
        return ResponseEntity.ok(accountService.findAll());
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<AccountVO> findById(@PathVariable Long id) {
        Optional<AccountVO> account = accountService.findById(id);
        if (!account.isPresent()) {
        	log.error("account with id: " + id + " does not exist");
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(account.get());
    }

	@GetMapping("/email/{email}")
    public ResponseEntity<List<AccountVO>> findByEmail(@PathVariable String email) {
		List<AccountVO> accountList = accountService.findByEmail(email);
        return ResponseEntity.ok(accountList);
    }
	
	@PostMapping
    public ResponseEntity<AccountVO> create(@Valid @RequestBody AccountVO account) {
        return ResponseEntity.ok(accountService.save(account));
    }
	
    @PutMapping("/{id}")
    public ResponseEntity<AccountVO> update(@PathVariable Long id, @Valid @RequestBody AccountVO account) {
        if (!accountService.findById(id).isPresent()) {
            log.error("account with id: " + id + " does not exist");
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(accountService.save(account, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<AccountVO> delete(@PathVariable Long id) {
        if (!accountService.findById(id).isPresent()) {
            log.error("account with id: " + id + " does not exist");
            return ResponseEntity.badRequest().build();
        }
        accountService.deleteById(id);
        return ResponseEntity.ok().build();
    }
	
}
