package com.wefox.challenge.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wefox.challenge.service.PokemonService;
import com.wefox.challenge.vo.PokemonVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/pokemons")
@Slf4j
@RequiredArgsConstructor
public class PokemonApi {
	private final PokemonService pokemonService;

	@GetMapping("/prefix/{parameter}")
    public ResponseEntity<List<PokemonVO>> findByStatingWith(@PathVariable String parameter) {
		List<PokemonVO> pokemons = pokemonService.findByPrefix(parameter);
		log.debug("with the prefix: '" + parameter + "', " + pokemons.size() + " results were found.");
        return ResponseEntity.ok(pokemons);
    }
	
}
