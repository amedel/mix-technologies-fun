package com.wefox.challenge.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wefox.challenge.rabbitmq.PokemonNameProducer;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/pokemons/queue")
@RequiredArgsConstructor
public class RabbitAPI {

	private final PokemonNameProducer pokemonNameProducer;

	@PostMapping("/prefix/{prefix}")
	public ResponseEntity<Void> sendMessagetoQueu(@PathVariable String prefix) {
		pokemonNameProducer.pushWithoutCache(prefix);
		return ResponseEntity.ok().build();
	}

}
