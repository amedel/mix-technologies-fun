package com.wefox.challenge.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wefox.challenge.service.ThreadService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/thread")
@RequiredArgsConstructor
public class ThreadApi {
	private final ThreadService threadService;
	
	
	@PostMapping
    public ResponseEntity<Void> launch() {
		threadService.launch();
        return ResponseEntity.ok().build();
    }
	
}
