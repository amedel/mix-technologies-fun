package com.wefox.challenge.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.gateway.MessagingGatewaySupport;
import org.springframework.integration.http.inbound.HttpRequestHandlingMessagingGateway;
import org.springframework.integration.http.inbound.RequestMapping;
import org.springframework.integration.http.support.DefaultHttpHeaderMapper;
import org.springframework.integration.mapping.HeaderMapper;

@Configuration
@ComponentScan("com.wefox.challenge.integration")
@EnableIntegration
public class InfrastructureConfiguration {

	@Bean
	public MessagingGatewaySupport httpGetGate() {
	    HttpRequestHandlingMessagingGateway handler = new HttpRequestHandlingMessagingGateway();
	    handler.setRequestMapping(createMapping(new HttpMethod[]{HttpMethod.GET}, "/api/v1/toy/{id}"));
	    handler.setPayloadExpression(parser().parseExpression("#pathVariables.id"));
	    handler.setHeaderMapper(headerMapper());
	    
	    return handler;
	}
	
	@Bean
	public IntegrationFlow httpGetFlow() {
	    return IntegrationFlows.from(httpGetGate()).channel("httpGetChannel").handle("toyEndpoint", "get").get();
	}
	
	@Bean
    public ExpressionParser parser() {
        return new SpelExpressionParser();
    }
    
    @Bean
    public HeaderMapper<HttpHeaders> headerMapper() {
        return new DefaultHttpHeaderMapper();
    }

	private RequestMapping createMapping(HttpMethod[] method, String... path) {
	    RequestMapping requestMapping = new RequestMapping();
	    requestMapping.setMethods(method);
	    requestMapping.setPathPatterns(path);
	    
	    return requestMapping;
	}
	
}
