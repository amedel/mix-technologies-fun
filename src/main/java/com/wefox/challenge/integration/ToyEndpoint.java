package com.wefox.challenge.integration;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.integration.http.HttpHeaders;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.wefox.challenge.vo.ToyVO;

@Component
public class ToyEndpoint {
	private static final String STATUSCODE_HEADER = "status";

	private static final List<ToyVO> toyDictionary = Arrays.asList(ToyVO.builder().id(1L).name("kite").build(),
																	ToyVO.builder().id(2L).name("teddy bear").build(),
																	ToyVO.builder().id(3L).name("doll").build(),
																	ToyVO.builder().id(4L).name("ruber duck").build());

	public Message<?> get(Message<String> msg) {
        final long id = Long.valueOf(msg.getPayload());
        Optional<ToyVO> toyVO = getToy(id);
        
        if (!toyVO.isPresent()) {
            return MessageBuilder.withPayload("no toy found with id: " + msg.getPayload())
                .copyHeadersIfAbsent(msg.getHeaders())
                .setHeader(HttpHeaders.STATUS_CODE, HttpStatus.NOT_FOUND)
                .build(); 
        }
        
        return MessageBuilder.withPayload(toyVO)
            .copyHeadersIfAbsent(msg.getHeaders())
            .setHeader(STATUSCODE_HEADER, HttpStatus.OK)
            .build();
    }
	
	private Optional<ToyVO> getToy(final Long id) {
		return toyDictionary.stream().filter(toy -> toy.getId().equals(id)).findAny();
	}
	
}
