package com.wefox.challenge.rabbitmq;

import java.util.Comparator;
import java.util.List;

import com.wefox.challenge.service.PokemonService;
import com.wefox.challenge.vo.PokemonVO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class PokemonNameConsumer {

	public static final String RECEIVE_METHOD_NAME = "receiveMessage";
	private final PokemonService pokemonService;
	
	public void receiveMessage(String message) {
		log.info("Message received: " + message);
		log.info("Looking for pokemons with that prefix...");
		List<PokemonVO> pokemonsNames = pokemonService.findByPrefix(message);
		log.info(pokemonsNames.size() + " pokemons were found with prefix: " + message);
		pokemonsNames.stream()
		             .sorted(Comparator.comparing(PokemonVO::getName))
		             .forEach(pokemonVO -> log.info(pokemonVO.getName()));
    }
}
