package com.wefox.challenge.rabbitmq;

import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PokemonNameProducer {

	@Autowired
	RabbitTemplate rabbitTemplate;

	public void pushWithoutCache(String prefix) {
		log.info("Sending the message to queu. -> " + prefix);
		rabbitTemplate.convertAndSend(RabbitMqConfiguration.EXCHANGE_NAME, RabbitMqConfiguration.ROUTING_KEY, prefix,
				message -> {
					message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.NON_PERSISTENT);
					return message;
				});
	}

}
