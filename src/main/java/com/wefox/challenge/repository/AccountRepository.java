package com.wefox.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wefox.challenge.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

}
