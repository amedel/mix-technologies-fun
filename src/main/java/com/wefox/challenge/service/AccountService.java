package com.wefox.challenge.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wefox.challenge.model.Account;
import com.wefox.challenge.model.Address;
import com.wefox.challenge.repository.AccountRepository;
import com.wefox.challenge.vo.AccountVO;
import com.wefox.challenge.vo.AddressVO;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class AccountService {
	private final AccountRepository accountRepository;

	public List<AccountVO> findAll() {
		return accountRepository.findAll().stream().map(this::getAccountVO).collect(Collectors.toList());
	}

	public Optional<AccountVO> findById(Long id) {
		return accountRepository.findById(id).map(this::getAccountVO);
	}

	public List<AccountVO> findByEmail(String email) {
		return accountRepository.findAll(Example.of(Account.builder().email(email).build())).stream()
				.map(this::getAccountVO).collect(Collectors.toList());
	}

	public AccountVO save(AccountVO accountVO) {
		return save(accountVO, null);
	}

	public AccountVO save(AccountVO accountVO, Long id) {
		AccountVO accountVOResponse = getAccountVO(accountRepository.save(getAccount(accountVO, id)));
		accountVOResponse.setAddresses(null);
		return accountVOResponse;
	}

	public void deleteById(Long id) {
		accountRepository.deleteById(id);
	}

	private Account getAccount(AccountVO accountVO, Long id) {
		return Account.builder().id(id).name(accountVO.getName()).email(accountVO.getEmail()).age(accountVO.getAge())
				.addresses(getAdresses(accountVO.getAddresses())).build();
	}

	private List<Address> getAdresses(List<AddressVO> addressesVO) {
		return addressesVO.stream().map(addressVO -> Address.builder().address(addressVO.getAddress()).build())
				.collect(Collectors.toList());
	}

	private AccountVO getAccountVO(Account account) {
		return AccountVO.builder().id(account.getId()).name(account.getName()).email(account.getEmail())
				.age(account.getAge()).addresses(getAdressesVO(account.getAddresses())).build();
	}

	private List<AddressVO> getAdressesVO(List<Address> addresses) {
		return addresses.stream().map(address -> AddressVO.builder().address(address.getAddress()).build())
				.collect(Collectors.toList());
	}

}
