package com.wefox.challenge.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.wefox.challenge.vo.PokemonVO;

import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResource;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResourceList;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;

@Service
public class PokemonService {

	public List<PokemonVO> findByPrefix(final String prefix){
        final List<PokemonVO> pokemonVOList = new ArrayList<>();
        PokeApi pokeApi = new PokeApiClient();
        NamedApiResourceList namedApiResourceList = pokeApi.getPokemonList(0,-1);
        pokemonVOList.addAll(namedApiResourceList.getResults().stream()
												        .filter(apiResource->apiResource.getName().toLowerCase().startsWith(prefix.trim().toLowerCase()))
												        .map(apiResorce->getPokemonVO(apiResorce, pokeApi))
												        .collect(Collectors.toList()));
        return pokemonVOList;
	}

	private PokemonVO getPokemonVO(NamedApiResource apiResorce, PokeApi pokeApi) {
		Pokemon pokemon = pokeApi.getPokemon(apiResorce.getId());
		PokemonVO pokemonVO = PokemonVO.builder().name(pokemon.getName())
				.weight(pokemon.getWeight())
				.height(pokemon.getHeight()) 
				.abilities(pokemon.getAbilities().stream()
						                         .map(abilityResource->pokeApi.getAbility(abilityResource.getAbility().getId()).getName())
						                         .collect(Collectors.toList()))
				.build();
		return pokemonVO;
	}
}
