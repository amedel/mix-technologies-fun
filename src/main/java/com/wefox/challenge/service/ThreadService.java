package com.wefox.challenge.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import com.wefox.challenge.thread.PokemonHunter;
import com.wefox.challenge.thread.StoragePokemon;

@Service
public class ThreadService {
	private static final Executor executor = Executors.newCachedThreadPool();
	
	public FutureTask<Void> launch() {
		ArrayList<CompletableFuture<String>> completableFutureArray = new ArrayList<>();
		IntStream.range(0, 3).mapToObj(i -> CompletableFuture.supplyAsync(new PokemonHunter(i)))
				.forEach(completableFuture -> completableFutureArray.add(completableFuture));
		
		FutureTask<Void> storagePokemonTask = new FutureTask<>(new StoragePokemon(
				completableFutureArray.toArray(new CompletableFuture[completableFutureArray.size()])));
		executor.execute(storagePokemonTask);

		return storagePokemonTask;
		
	}

}
