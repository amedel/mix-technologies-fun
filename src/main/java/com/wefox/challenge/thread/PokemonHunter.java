package com.wefox.challenge.thread;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

import lombok.extern.slf4j.Slf4j;
import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;

@Slf4j
public class PokemonHunter implements Supplier<String>{

	private int hunderId;
	
	public PokemonHunter(int hunderId) {
		super();
		this.hunderId = hunderId;
	}

	@Override
	public String get() {
		PokeApi pokeApi = new PokeApiClient();
		int randomId = ThreadLocalRandom.current().nextInt(1, 893);
		log.debug("hunter "+hunderId+" looking for pokemonId: "+ randomId);
		Pokemon pokemon = pokeApi.getPokemon(randomId);
		log.debug("hunter"+hunderId+" has finished");
		return pokemon.getName() ;
	}
}
