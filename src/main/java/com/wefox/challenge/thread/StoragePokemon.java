package com.wefox.challenge.thread;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StoragePokemon implements Callable<Void> {
	private CompletableFuture<?>[] hunters;
	private final List<SimpleEntry<String, Integer>> pokemonDictionary = new ArrayList<>();

	public StoragePokemon(CompletableFuture<?>... hunters ) {
		this.hunters = hunters;
	}
	
	@Override
	public Void call() {
		CompletableFuture<Void> completableFuture = CompletableFuture.allOf(hunters);
		log.debug("Pokemon Storage started...");
		try {
			log.debug("Pokemon Storage waiting for hunters finished...");
			completableFuture.get(5L,TimeUnit.SECONDS);
		} catch (Exception e) {
			log.error("Something was wrong while waiting for hunters");
			log.error("Perhaps one or more random pokemons don't exist (404)", e);
		}
		
		if(!completableFuture.isCompletedExceptionally()) {
			log.debug("hunters had finished, Pokemon Storage will continue...");
			Stream.of(hunters).forEach(this::addPokemon);
			pokemonDictionary.stream()
			                 .collect(Collectors.groupingBy(SimpleEntry::getKey, Collectors.summingInt(SimpleEntry::getValue)))
			                 .entrySet().stream().forEach(entry -> log.info("I got: "+ entry.getValue() + " of " + entry.getKey()));
		}
		return null;
	}
	
	private void addPokemon(CompletableFuture<?> pokemonFuture) {
		try {
			pokemonDictionary.add(new SimpleEntry<String, Integer>(pokemonFuture.get().toString(),1));
		} catch (Exception e) {
			log.error("Could not get the value from completableFuture", e);
		}
	}

	
}
