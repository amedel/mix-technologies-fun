package com.wefox.challenge.vo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressVO {
	private String address;

	@JsonCreator
	public AddressVO(@JsonProperty("address") String address) {
		this.address = address;
	}
	
	
	
}
