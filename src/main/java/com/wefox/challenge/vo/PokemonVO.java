package com.wefox.challenge.vo;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PokemonVO {
	private String name;
	private Integer weight;
	private Integer height;
	private List<String> abilities;
}
