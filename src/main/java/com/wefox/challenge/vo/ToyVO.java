package com.wefox.challenge.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ToyVO {
	private Long id	;
	private String name;
}
