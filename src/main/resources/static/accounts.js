var accounts = [];

function findAccount (accountId) {
  return accounts[findAccountKey(accountId)];
}

function findAccountKey (accountId) {
  for (var key = 0; key < accounts.length; key++) {
    if (accounts[key].id == accountId) {
      return key;
    }
  }
}

var accountService = {
  findAll(fn) {
    axios
      .get('/api/v1/accounts')
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  findById(id, fn) {
    axios
      .get('/api/v1/accounts/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  create(account, fn) {
    axios
      .post('/api/v1/accounts', account)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },


  update(id, account, fn) {
	    axios
	      .put('/api/v1/accounts/' + id, account)
	      .then(response => fn(response))
	      .catch(error => console.log(error))
	      .then(response => {console.log(response.status)}) 	
   },

  deleteAccount(id, fn) {
    axios
      .delete('/api/v1/accounts/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
      .then(fn())
  }
}

var List = Vue.extend({
  template: '#list-account',
  data: function () {
    return {accounts: [], searchKey: ''};
  },
  computed: {
    filteredAccounts() {
      return this.accounts.filter((account) => {
      	return account.name.indexOf(this.searchKey) > -1
      	  || account.email.indexOf(this.searchKey) > -1
      })
    }
  },
  mounted() {
    accountService.findAll(r => {this.accounts = r.data; accounts = r.data})
  }
});

var Account = Vue.extend({
  template: '#account',
  data: function () {
    return {account: findAccount(this.$route.params.account_id)};
  }
});

var AccountEdit = Vue.extend({
  template: '#edit-account',
  data: function () {
	  return {account: findAccount(this.$route.params.account_id)};
  },
  methods: {
    updateAccount: function () {
   	  this.account.addresses = this.account.addresses.filter(addressObj=>addressObj.address.trim())
      accountService.update(this.account.id, this.account, r => router.push({name:'list-account'}))
    },
    addAddress(){
    	this.account.addresses.push({'address':''});
    }
  }
});

var AccountDelete = Vue.extend({
  template: '#delete-account',
  data: function () {
	  return {account: findAccount(this.$route.params.account_id)};
  },
  methods: {
    deleteAccount: function () {
    	accountService.deleteAccount(this.account.id, r => router.push('/'))
    }
  }
});

var AddAccount = Vue.extend({
  template: '#add-account',
  data() {
    return {
      account: {name: '', email: '', age: 0, addresses:[]}
    }
  },
  methods: {
    createAccount() {
   	  this.account.addresses = this.account.addresses.filter(addressObj=>addressObj.address.trim())	
      accountService.create(this.account, r => router.push('/'))
    },
    addAddress(){
    	this.account.addresses.push({'address':''});
    }
  }
});

var router = new VueRouter({
	routes: [
		{path: '/', component: List, name: 'list-account'},
		{path: '/account/:account_id', component: Account, name: 'account'},
		{path: '/add-account', component: AddAccount},
		{path: '/account/:account_id/edit', component: AccountEdit, name: 'edit-account'},
		{path: '/account/:account_id/delete', component: AccountDelete, name: 'delete-account'}
	]
});

new Vue({
  router
}).$mount('#app')
