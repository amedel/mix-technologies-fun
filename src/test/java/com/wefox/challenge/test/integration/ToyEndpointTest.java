package com.wefox.challenge.test.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.wefox.challenge.vo.ToyVO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ToyEndpointTest {

	@Autowired
    private TestRestTemplate template;
	
	@Test
	public void should_return_a_toy() {
		ResponseEntity<ToyVO> result = template.withBasicAuth("user", "password")
		          .getForEntity("/api/v1/toy/1", ToyVO.class);
		assertNotNull(result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(1L, result.getBody().getId().longValue());
		assertEquals("kite", result.getBody().getName());
	}
	
	@Test
	public void should_not_return_a_toy() {
		ResponseEntity<String> result = template.withBasicAuth("user", "password")
		          .getForEntity("/api/v1/toy/10", String.class);
		assertNotNull(result);
		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
		assertEquals("no toy found with id: 10", result.getBody());
		
	}	
	
}
