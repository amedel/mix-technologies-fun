package com.wefox.challenge.test.security;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.wefox.challenge.vo.AccountVO;
import com.wefox.challenge.vo.PokemonVO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class WebSecurityTest {
 
    @Autowired
    private TestRestTemplate template;
 
    @Test
    public void givenAuthRequestOnPrivateService_shouldSucceedWith200() throws Exception {
		ResponseEntity<List<PokemonVO>> result = template.withBasicAuth("user", "password")
          .getForEntity("/api/v1/pokemons/prefix/pika", null, List.class);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    
    @Test
    public void givenAuthRequestOnPrivateService_shouldFail() throws Exception {
        ResponseEntity<Void> result = template.postForEntity("/api/v1/thread", null, Void.class);
        assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
    }
    
    @Test
    public void givenAuthRequestOnPublicService_shouldSucceedWith200() throws Exception {
        ResponseEntity<List<AccountVO>> result = template.getForEntity("/api/v1/accounts", null, List.class);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}
