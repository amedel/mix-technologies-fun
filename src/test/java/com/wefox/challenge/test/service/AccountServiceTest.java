package com.wefox.challenge.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.wefox.challenge.service.AccountService;
import com.wefox.challenge.vo.AccountVO;
import com.wefox.challenge.vo.AddressVO;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest()
public class AccountServiceTest {

	@Autowired
	AccountService accountService;

	AccountVO accountVO;

	@Before
	public void setup() {
		accountVO = AccountVO.builder().age(5).name("pruebaTest").email("pruebatest@email.com")
				.addresses(Arrays.asList(AddressVO.builder().address("address Test").build())).build();

	}

	@Test
	public void should_return_some_accounts() {
		List<AccountVO> accountList = accountService.findAll();
		assertNotNull(accountList);
		assertFalse(accountList.isEmpty());
	}

	@Test
	public void should_return_one_accounts_by_id() {
		Optional<AccountVO> account = accountService.findById(11L);
		assertNotNull(account);
		assertTrue(account.isPresent());
		assertTrue(account.get().getId() == 11);
		assertEquals("testname2", account.get().getName());
	}

	@Test
	public void should_return_one_accounts_by_email() {
		List<AccountVO> accountList = accountService.findByEmail("testname2@test.com");
		assertNotNull(accountList);
		assertFalse(accountList.isEmpty());
		assertTrue(accountList.get(0).getId() == 11);
	}

	@Test
	public void should_create_and_account() {
		AccountVO account = accountService.save(accountVO);
		assertNotNull(account);
		assertNotNull(account.getId());
		assertNull(account.getAddresses());
		assertEquals("pruebaTest", account.getName());
	}

	@Test
	public void should_update_and_account() {
		AccountVO account = accountService.save(accountVO, 1L);
		assertNotNull(account);
		assertNotNull(account.getId());
		assertEquals(1L, account.getId().longValue());
		assertNull(account.getAddresses());
		assertEquals("pruebaTest", account.getName());
	}

	@Test
	public void should_return_nothing_after_delete() {
		accountService.deleteById(1L);
		Optional<AccountVO> account = accountService.findById(1L);
		assertNotNull(account);
		assertFalse(account.isPresent());
	}

}
