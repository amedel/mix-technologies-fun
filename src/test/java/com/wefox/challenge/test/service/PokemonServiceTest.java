package com.wefox.challenge.test.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import com.wefox.challenge.service.PokemonService;
import com.wefox.challenge.vo.PokemonVO;

@ActiveProfiles("test")
public class PokemonServiceTest {

	PokemonService pokemonService = new PokemonService();
	
	@Test
	public void should_return_some_results(){
		List<PokemonVO> pokemonsList = pokemonService.findByPrefix("pika");
		assertNotNull(pokemonsList);
		assertFalse(pokemonsList.isEmpty());
		assertTrue(pokemonsList.stream().anyMatch(pokemon->pokemon.getName().equalsIgnoreCase("pikachu")));
	}
}
