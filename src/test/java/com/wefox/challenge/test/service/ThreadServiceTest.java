package com.wefox.challenge.test.service;

import java.util.concurrent.FutureTask;

import org.junit.Assert;
import org.junit.Test;

import com.wefox.challenge.service.ThreadService;


public class ThreadServiceTest {

	ThreadService threadService = new ThreadService();

	@Test
	public void testLaunch() {
		FutureTask<Void> storagePokemonTask = threadService.launch(); 
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			Assert.assertNull(e.getMessage(), e); 
		}
		Assert.assertTrue(storagePokemonTask.isDone()); 
		Assert.assertFalse(storagePokemonTask.isCancelled());
	}
	
}
